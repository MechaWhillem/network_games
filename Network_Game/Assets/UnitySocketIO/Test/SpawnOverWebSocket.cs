﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnitySocketIO;
using UnitySocketIO.Events;

public class SpawnOverWebSocket : MonoBehaviour
{
    public GameObject[] objectToSpawn;
    private bool B_AreHere = false;

    SocketIOController io;
    // Start is called before the first frame update
    void Start()
    {
        io = SocketIOController.instance;
        io.On("connect", (SocketIOEvent e) => {
            Debug.Log("SocketIO connected");
        });

        io.Connect();

        io.On("spawn", (SocketIOEvent e) => {

            for (int i = 0; i < transform.childCount; i++)
            {
                if (transform.GetChild(i).name == e.data)
                {
                    B_AreHere = true;
                    break;
                }
            }

            if (B_AreHere == false)
            {
                GameObject G_inst = Instantiate(objectToSpawn[Random.Range(0, objectToSpawn.Length)], new Vector3(Random.Range(-7, 7), Random.Range(-4, 0), 0), Quaternion.identity, transform);
                G_inst.name = e.data;
                G_inst.GetComponent<PlayerScore>().S_Name = e.data.Trim('"');
                GameObject.FindGameObjectWithTag("GM").GetComponent<GameManager>().AddPlayers(G_inst);
            }
        });
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}

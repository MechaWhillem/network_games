﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.SceneManagement;

public class IAManager : MonoBehaviour
{

    public GameObject[] G_IAV1;
    public GameObject[] G_IAV2;
    public GameObject[] G_IAV3;
    //public GameObject G_IABoss;

    private float F_Timer;
    private int I_VagueTime = 30;
    public bool B_StartWave = false;
    public bool B_WaveEnd = false;
    public bool B_InWave = false;
    public GameObject G_CurrentIA;
    public GameObject G_AllPlayerParent;

    GameManager gm;

    public Animation EndingScore;
    public TextMeshProUGUI TMP_TextEnd;

    private void Start()
    {
        gm = GetComponent<GameManager>();
        F_Timer = I_VagueTime;
        StartWave();
    }

    private void Update()
    {
        if (F_Timer <= 0 && B_WaveEnd == false)
        {
            EndWave();
            B_WaveEnd = true;
        }

        if (B_StartWave == true)
        {
            if (F_Timer >= 0)
            {
                F_Timer -= Time.deltaTime;
                gm.UI_Slider_TimeEnemies1.value = F_Timer;
                gm.UI_Slider_TimeEnemies2.value = F_Timer;
                B_WaveEnd = false;
            }
        }
    }

    IEnumerator WaitStart()
    {
        gm.AnimSlider();
        yield return new WaitForSeconds(1);
        F_Timer = I_VagueTime;
        B_StartWave = true;
    }

    public void StartWave()
    {
        int rad = Random.Range(0, 3);

        if (gm.I_Level <=5)
        {
            G_CurrentIA = Instantiate(G_IAV1[rad], transform.position, Quaternion.identity);
            StartCoroutine(WaitStart());
        }
        else if (gm.I_Level <= 10)
        {
            G_CurrentIA = Instantiate(G_IAV2[rad], transform.position, Quaternion.identity);
            StartCoroutine(WaitStart());
        }
        else if (gm.I_Level <= 15)
        {
            G_CurrentIA = Instantiate(G_IAV3[rad], transform.position, Quaternion.identity);
            StartCoroutine(WaitStart());
        } else
        {
            StartCoroutine(EndGame());
            
        }
      
    }

    IEnumerator WaitEnd()
    {
        yield return new WaitForSeconds(1);
        gm.AddLevel();
        yield return new WaitForSeconds(1);      
        StartWave();
    }

    public void EndWave()
    {
        GameObject.FindGameObjectWithTag("IA").GetComponent<EnemyIA>().EndAnimCouroutine();
        StartCoroutine(WaitEnd());
    }

    public void ResetGame()
    {
        gm.I_Level = 0;
        gm.TMP_levelDisplay.text = "LVL : END";
    }

    IEnumerator EndGame()
    {
        ResetGame();
        TMP_TextEnd.text = "Best Player : " + gm.G_TopPlayer.name.Trim('"');
        EndingScore.Play();
        yield return new WaitForSeconds(8);        
        SceneManager.LoadScene("Game");

    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class PlayerScore : MonoBehaviour
{
    public float F_Score = 0;
    public string S_Name;
    public TextMeshProUGUI TMP_PlayerNameDisplay;
    public Canvas Canvas_UIPlayer;

    public float PPS; //PPS = point per second

    public float pourcentage;
    public float scale;

    // Start is called before the first frame update
    void Start()
    {
        F_Score = 0f;
        PPS = 0.5f;
        TMP_PlayerNameDisplay.text = S_Name;
    }

    // Update is called once per frame
    void Update()
    {
        F_Score += PPS * Time.deltaTime;
        F_Score = Mathf.Clamp(F_Score, 0, 100000000000);

        scale = pourcentage * F_Score;

        if (scale + 1 >= 7)
        {

        }
        else
        {
            transform.localScale = new Vector3(scale + 1, scale + 1, 0);
            //Canvas_UIPlayer.transform.localScale = new Vector3((scale *0.12f)+1, (scale *0.12f)+1, 0);
        }
    }
}

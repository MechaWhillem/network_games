﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProjectileIdle: MonoBehaviour
{
    public GameObject G_DestroyEffect;
    public float speed = 2;
    public int timedeath = 2;
    public int Damage = 1;
    public bool B_CanBeDestroy = true;

    void Start()
    {
        if (B_CanBeDestroy == true)
        {
            Destroy(gameObject, timedeath);
        }
        
    }

    void FixedUpdate()
    {
        GetComponent<Rigidbody2D>().AddForce(transform.up *speed);
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (B_CanBeDestroy == true)
        {
            Destroy(gameObject);
        }
        
    }

    private void OnDestroy()
    {
        //Instantiate(G_DestroyEffect, transform.position, Quaternion.identity);
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour
{
    public GameObject comete;
    public GameObject star;

    private void Start()
    {
        StartCoroutine(Spawn());
    }


    IEnumerator Spawn()
    {
        int rad = Random.Range(2, 5);
        yield return new WaitForSeconds(rad);
        int rad2 = Random.Range(0,2);
        if (rad2 == 1)
        {
            Instantiate(comete, new Vector3(Random.Range(-7.5f, 7.5f), transform.position.y, 0), Quaternion.identity);
        } else
        {
            Instantiate(star, new Vector3(Random.Range(-7.5f, 7.5f), transform.position.y, 0), Quaternion.identity);
        }
        StartCoroutine(Spawn());
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using DG.Tweening;

public class GameManager : MonoBehaviour
{
    public Dictionary<int, GameObject> G_Players = new Dictionary<int, GameObject>();
    public int I_NmbOfPlayers = 0;

    public Slider UI_Slider_TimeEnemies1;
    public Slider UI_Slider_TimeEnemies2;
    public TextMeshProUGUI TMP_NmbOfPlayerDisplay;
    public TextMeshProUGUI TMP_BestPlayerDisplay;
    public TextMeshProUGUI TMP_TimerDisplay;
    public TextMeshProUGUI TMP_levelDisplay;
    public GameObject G_Canvas;

    public AudioClip AC_Lvl1;
    public AudioClip AC_Lvl2;
    public AudioClip AC_Lvl3;

    public GameObject G_Star1;
    public GameObject G_Star2;
    public GameObject G_Star3;

    public int I_Level = 1;
    public GameObject G_TopPlayer;
    private float F_Timer;
    public GameObject G_Spawner;
    public AudioSource AS_LevelingSound;

    public bool B_HavePlayer = false;
    private bool B_StartGame = false;

    private void Start()
    {
        UI_Slider_TimeEnemies1.maxValue = 30;
        UI_Slider_TimeEnemies2.maxValue = 30;
        GetComponent<AudioSource>().DOFade(0.2f, 5);
        GetComponent<AudioSource>().clip = AC_Lvl1;
        GetComponent<AudioSource>().Play();
    }

    private void Update()
    {
        if (GetComponent<IAManager>().G_AllPlayerParent.transform.childCount > 0 && B_HavePlayer == false)
        {
            StartCoroutine(StartGame());
            B_HavePlayer = true;
        }

        if (B_StartGame == true)
        {
            TMP_TimerDisplay.text = "Time : " + Mathf.RoundToInt(F_Timer);
            F_Timer += Time.deltaTime;
        }

        //check the best score
        for (int i = 1; i < I_NmbOfPlayers; i++)
        {
            if (G_TopPlayer.GetComponent<PlayerScore>().F_Score < G_Players[i].GetComponent<PlayerScore>().F_Score)
            {
                G_TopPlayer = G_Players[i];
                break;
            }
        }
        if (I_NmbOfPlayers > 0)
        {
            if (G_TopPlayer !=null)
            {
                TMP_BestPlayerDisplay.text = "Best Player : " + G_TopPlayer.GetComponent<PlayerScore>().S_Name;
            }
        }
        else
        {
            TMP_BestPlayerDisplay.text = "Best Player : ?????";
        }
    }

    IEnumerator StartGame()
    {
        G_Canvas.GetComponent<Animator>().Play("Start Game2");
        yield return new WaitForSeconds(2f);
        GetComponent<IAManager>().enabled = true;
        G_Spawner.SetActive(true);
        B_StartGame = true;
    }

    public void AnimSlider()
    {
        UI_Slider_TimeEnemies1.DOValue(30,1);
        UI_Slider_TimeEnemies1.gameObject.GetComponent<Animator>().Play("NextLevel");
        UI_Slider_TimeEnemies2.DOValue(30, 1);
        UI_Slider_TimeEnemies2.gameObject.GetComponent<Animator>().Play("NextLevel");
    }

    public void AddLevel()
    {
        I_Level++;
        TMP_levelDisplay.text = "LVL : " + I_Level;
        TMP_levelDisplay.gameObject.GetComponent<Animator>().Play("AddLevel");
        AS_LevelingSound.Play();
        if (I_Level == 6)
        {
            StartCoroutine(ChangeMusic(AC_Lvl2));
            G_Star1.GetComponent<ParticleSystem>().Stop();
            G_Star2.SetActive(true);
        }
        if (I_Level == 11)
        {
            StartCoroutine(ChangeMusic(AC_Lvl3));
            G_Star2.GetComponent<ParticleSystem>().Stop();
            G_Star3.SetActive(true);
        }
    }

    IEnumerator ChangeMusic(AudioClip AC_Clip)
    {
        GetComponent<AudioSource>().DOFade(0,1);
        yield return new WaitForSeconds(1f);
        GetComponent<AudioSource>().clip = AC_Clip;
        GetComponent<AudioSource>().Play();
        GetComponent<AudioSource>().volume = 0.2f;
    }

    public void AddPlayers(GameObject G_Player)
    {
        
        I_NmbOfPlayers++;
        G_Players.Add(I_NmbOfPlayers, G_Player);
        G_Player.transform.GetChild(0).name = "Player"+I_NmbOfPlayers;
        TMP_NmbOfPlayerDisplay.text = "Players : " + I_NmbOfPlayers;

        if (I_NmbOfPlayers == 1)
        {
            G_TopPlayer = G_Players[1];
        }
    }

    public void RemovePlayers(GameObject G_Player)
    {
        int I_ParsName;
        int.TryParse(G_Player.transform.GetChild(0).name, out I_ParsName);
        G_Players.Remove(I_ParsName);
        I_NmbOfPlayers--;
        TMP_NmbOfPlayerDisplay.text = "Players : " + I_NmbOfPlayers;
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShotIA : MonoBehaviour
{
    private float timeBTWShots;
    public float startTimeBTWShots;

    public GameObject projectile;
    public GameObject ShotPoint;

    private void Start()
    {
        timeBTWShots = startTimeBTWShots;
    }

    void Update()
    {
        if (transform.parent.parent.GetComponent<EnemyIA>().B_Stop == false)
        {
            if (timeBTWShots <= 0)
            {
                Instantiate(projectile, ShotPoint.transform.position, Quaternion.Euler(0, 0, -180));
                timeBTWShots = startTimeBTWShots;
            }
            else
            {
                timeBTWShots -= Time.deltaTime;
            }
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class EnemyIA : MonoBehaviour
{
    private float move;
    public float speed = 0.6f;

    public bool right = false;
    public bool left = false;
    public bool B_Stop = false;

    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(StartAnim());
    }

    // Update is called once per frame
    void Update()
    {
       
        if (right == true)
        {
            transform.position += Vector3.right * speed * Time.deltaTime;
        }

        if (left == true)
        {
            transform.position += Vector3.left * speed * Time.deltaTime;
        }

        transform.position = new Vector3(Mathf.Clamp(transform.position.x, -8, 8), transform.position.y, transform.position.x);

    }

    public void EndAnimCouroutine()
    {
        StartCoroutine(EndAnim());
    }

    IEnumerator StartAnim()
    {
        transform.DOMoveY(3.6f, 1);
        yield return new WaitForSeconds(1);
        StartCoroutine(randomEnemy());
    }

    IEnumerator EndAnim()
    {
        B_Stop = true;
        right = false;
        left = false;
        transform.DOMoveY(6f, 1);
        yield return new WaitForSeconds(1);
        Destroy(gameObject);
    }

    IEnumerator randomEnemy()
    {
        if (B_Stop == false)
        {
            speed = Random.Range(1.2f, 2.4f);
            int rad = Random.Range(0, 2);
            if (rad == 1)
            {
                right = true;
            }
            else
            {
                left = true;
            }
            yield return new WaitForSeconds(Random.Range(0.5f, 3f));
            right = false;
            left = false;
            yield return new WaitForSeconds(Random.Range(0.5f, 1f));
            StartCoroutine(randomEnemy());
        }
    }


}

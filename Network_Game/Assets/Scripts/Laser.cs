﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Laser : MonoBehaviour
{
    private float timeBTWShots;
    public float startTimeBTWShots;

    private void Start()
    {
        timeBTWShots = startTimeBTWShots;
    }

    private void Update()
    {
        if (transform.parent.parent.GetComponent<EnemyIA>().B_Stop == false)
        {
            if (timeBTWShots <= 0)
            {
                GetComponent<Animator>().Play("IA laser");
                timeBTWShots = startTimeBTWShots;
            }
            else
            {
                timeBTWShots -= Time.deltaTime;
            }
        }
        else
        {
            GetComponent<Animator>().Play("New State");
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Collision : MonoBehaviour
{
    public GameObject G_DamageParticule;
    private bool B_takeDamage = false;

    private void Update()
    {
        if (transform.position.x >9.3f)
        {
            DestroyPlayer();
        }
        else if (transform.position.x < -9.3f)
        {
            DestroyPlayer();
        }
        else if (transform.position.y > 5.3f)
        {
            DestroyPlayer();
        }
        else if (transform.position.y < -5.3f)
        {
            DestroyPlayer();
        }
    }

    void DestroyPlayer()
    {
        GameObject.FindGameObjectWithTag("GM").GetComponent<GameManager>().RemovePlayers(gameObject);
        Destroy(gameObject);
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Comet"))
        {            
            GetComponent<MovingPlayer>().Comet();
            Destroy(collision.gameObject);
        }
        if (collision.gameObject.CompareTag("Etoile"))
        {
            GetComponent<PlayerScore>().F_Score += 2;
            GetComponent<Animator>().Play("Asteroid Score");
            Destroy(collision.gameObject);
        }
        if (collision.gameObject.CompareTag("Missile"))
        {
            GetComponent<PlayerScore>().F_Score -= collision.gameObject.GetComponent<ProjectileIdle>().Damage;
            Instantiate(G_DamageParticule, transform.position, Quaternion.identity);
        }
    }

    private void OnCollisionStay2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("laser"))
        {

            GetComponent<PlayerScore>().F_Score -= collision.gameObject.GetComponent<ProjectileIdle>().Damage;
            if (B_takeDamage == false)
            {
                StartCoroutine(DoDamage());
            }
        }

    }

    IEnumerator DoDamage()
    {
        if (B_takeDamage == false)
        {
            B_takeDamage = true;
            Instantiate(G_DamageParticule, transform.position, Quaternion.identity);
            yield return new WaitForSeconds(0.8f);
            B_takeDamage = false;
        }

    }
}

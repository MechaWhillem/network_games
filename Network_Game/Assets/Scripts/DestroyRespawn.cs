﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyRespawn : MonoBehaviour
{

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            GameObject.FindGameObjectWithTag("GM").GetComponent<GameManager>().RemovePlayers(collision.gameObject.transform.parent.gameObject);
            Destroy(collision.gameObject.transform.parent.gameObject);
        }
    }
}

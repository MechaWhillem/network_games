﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class Scoring : MonoBehaviour
{
    public Text ScoreText;

    public float scoreAmount;
    public float PPS; //PPS = point per second

    public GameObject Player;

    public float pourcentage;
    float scale;

    // Start is called before the first frame update
    void Start()
    {
        scoreAmount = 0f;
        PPS = 0.5f;
    }

    // Update is called once per frame
    void Update()
    {
        ScoreText.text = (int)scoreAmount + "";
        scoreAmount += PPS * Time.deltaTime;
        scoreAmount = Mathf.Clamp(scoreAmount,0,100000000000); 

        scale = pourcentage * scoreAmount;
        
        if (scale +1 >= 5)
        {

        }
        else
        {
            Player.transform.localScale = new Vector3(scale + 1, scale + 1, 0);
        }
    }
}

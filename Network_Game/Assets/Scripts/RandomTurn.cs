﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomTurn : MonoBehaviour
{
    private float move;
    private float speedRatio;

    private void Start()
    {
        if (Random.Range(0,1) == 1)
        {
            speedRatio = Random.Range(0.1f, 0.5f);
        }
        else
        {
            speedRatio = Random.Range(-0.1f, -0.5f);
        }
    }

    private void Update()
    {
        transform.rotation = Quaternion.Euler(0, 0, move += speedRatio);
    }
}

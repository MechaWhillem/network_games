﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnitySocketIO;
using UnitySocketIO.Events;

public class MovingPlayer : MonoBehaviour
{
    SocketIOController io;

    public GameObject player;
    public float speed = 8.0f;
    private Rigidbody2D rb;
    private bool B_InSpeed = false;

    private Vector2 moveVelocity;
    private Vector2 moveInput;

    private System.Action<SocketIOEvent> ActionHaut, ActionBas, ActionDroite, ActionGauche, ActionStop;

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();

        io = SocketIOController.instance;

        ActionHaut = (SocketIOEvent e) =>
        {
            if (e.data == gameObject.name)
            {
                moveInput = new Vector2(0, 1);
            }
        };

        ActionBas = (SocketIOEvent e) =>
        {
            if (e.data == gameObject.name)
            {
                moveInput = new Vector2(0, -1);
            }
        };

        ActionDroite = (SocketIOEvent e) =>
        {
            if (e.data == gameObject.name)
            {
                moveInput = new Vector2(1, 0);
            }
        };

        ActionGauche = (SocketIOEvent e) =>
        {
            if (e.data == gameObject.name)
            {
                moveInput = new Vector2(-1, 0);
            }
        };

        ActionStop = (SocketIOEvent e) =>
        {
            if (e.data == gameObject.name)
            {
                moveInput = new Vector2(0, 0);
            }
        };

        io.On("haut", ActionHaut);
        io.On("bas", ActionBas);
        io.On("droite", ActionDroite);
        io.On("gauche", ActionGauche);
        io.On("stop", ActionStop);
    }

    // Update is called once per frame
    void Update()
    {
        transform.position = new Vector3(Mathf.Clamp(transform.position.x, -8.5f, 8.5f), Mathf.Clamp(transform.position.y,-4.5f,2.5f), 0);
        moveVelocity = moveInput * speed;
        
        if (B_InSpeed == false)
        {
            speed = 8 / ((GetComponent<PlayerScore>().scale+1)/2);
        }

        speed = Mathf.Clamp(speed,2,10);
    }

    private void FixedUpdate()
    {
        rb.MovePosition(rb.position + moveVelocity * Time.fixedDeltaTime);
    }

    public void Comet()
    {
        StartCoroutine(SpeedBoost());
    }

    IEnumerator SpeedBoost()
    {
        B_InSpeed = true;
        speed *= 2f;
        GetComponent<Animator>().Play("Asteroid Speed");
        yield return new WaitForSeconds(3f);
        speed /=2f;
        B_InSpeed = false;
    }


    private void OnDestroy()
    {
        io.Off("haut", ActionHaut);
        io.Off("bas", ActionBas);
        io.Off("droite", ActionDroite);
        io.Off("gauche", ActionGauche);
        io.Off("stop", ActionStop);
    }
}


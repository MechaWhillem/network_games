﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShotIA2 : MonoBehaviour
{
    private float timeBTWShots;
    public float startTimeBTWShots;

    public GameObject projectile;
    public GameObject[] ShotPoint;

    private void Start()
    {
        timeBTWShots = startTimeBTWShots;
    }

    void Update()
    {

            if (timeBTWShots <= 0)
            {
                GameObject shot =  Instantiate(projectile, ShotPoint[0].transform.position,Quaternion.identity);
                shot.transform.rotation = ShotPoint[0].transform.rotation;
                /*Instantiate(projectile, ShotPoint[1].transform.position, Quaternion.Euler(0, 0, -180));
                Instantiate(projectile, ShotPoint[2].transform.position, Quaternion.Euler(0, 0, -180));
                Instantiate(projectile, ShotPoint[3].transform.position, Quaternion.Euler(0, 0, -180));
                Instantiate(projectile, ShotPoint[4].transform.position, Quaternion.Euler(0, 0, -180));
                Instantiate(projectile, ShotPoint[5].transform.position, Quaternion.Euler(0, 0, -180));
                Instantiate(projectile, ShotPoint[6].transform.position, Quaternion.Euler(0, 0, -180));
                Instantiate(projectile, ShotPoint[7].transform.position, Quaternion.Euler(0, 0, -180));*/

                timeBTWShots = startTimeBTWShots;
            }
            else
            {
                timeBTWShots -= Time.deltaTime;
            }

    }
}
